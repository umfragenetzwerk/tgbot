<?php

function addActivity($m)
{
    $db = getdb();

    if (checkUserConsent($m["from"]["id"]))
    {

        if ($m["chat"]["username"] != diskussionsgruppe and isset($m['poll']))
        {

            $sql = "SELECT * FROM activity WHERE userid = '" . $m["from"]["id"] . "' AND chat = '" . $m["chat"]["username"] . "' ORDER BY time DESC LIMIT 1";
            $statement = $db->prepare($sql);
            $statement->execute();
            $row = $statement->fetch();

            $last = 180;

            if (!empty($row))
            {
                //echo " - Last: ".$row["time"];
                $last = ($m["date"] - intval($row["time"])) / 60;
                //echo "- time: ".$last." - ";

            }

            if ($last > 180)
            {
                $last = 180;
            }

            $standard = 100;

            if ($m["chat"]["username"] == quiz)
            {
                $standard = 130;
            }

            $statement = $db->prepare("INSERT INTO activity (userid, chat, messageid, time, xp) VALUES (? , ?, ?, ?, ?)");
            $statement->execute(array(
                $m["from"]["id"],
                $m["chat"]["username"],
                $m["message_id"],
                $m["date"],
                ($last / 180) * $standard
            ));

        }
        else if (isset($m["text"]) and $m["chat"]["username"] == diskussionsgruppe)
        //$chat == "umfragediskussionen"

        {

            $sql = "SELECT * FROM activity WHERE userid = '" . $m["from"]["id"] . "' AND chat = '" . $m["chat"]["username"] . "' AND time > " . ($m["date"] - 3600);
            $collectedxp = 0;
            foreach ($db->query($sql) as $row)
            {
                $collectedxp = $collectedxp + $row["xp"];
            }

            $xp = count(explode(" ", str_replace("  ", " ", $m["text"]))) * 2;

            if ($collectedxp + $xp > 130)
            {
                $xp = 130 - $collectedxp;
            }

            //echo $xp;
            $statement = $db->prepare("INSERT INTO activity (userid, chat, messageid, time, xp) VALUES (? , ?, ?, ?, ?)");
            $statement->execute(array(
                $m["from"]["id"],
                $m["chat"]["username"],
                $m["message_id"],
                $m["date"],
                $xp
            ));

        }
    }
    if ($m["chat"]["username"] != diskussionsgruppe)
    {
        forewardMessage($m);
    }

    $db = null;

}

function deleteActivity($message)
{

    delAct($message["message_id"], $message["chat"]["username"]);

}

function delAct($message_id, $chat)
{
    $db = getdb();

    $statement = $db->prepare("DELETE FROM activity WHERE chat = ? AND messageid = ?");
    $statement->execute(array(
        $chat,
        $message_id
    ));
    $db = null;
}

function printLeaderboard()
{
    $db = getdb();

    //print top user for umfragen
    $sql = "SELECT * FROM activity WHERE chat = '" . umfragen . "' AND time > " . (time() - 604800);
    //echo $sql."</br>";
    $users = array();
    foreach ($db->query($sql) as $row)
    {
        //echo $row["userid"].": ".$row["xp"]."</br>";
        if (!isset($users[$row["userid"]]))
        {
            $users[$row["userid"]] = 1;
        }
        else
        {
            $users[$row["userid"]] = $users[$row["userid"]] + 1;
        }

    }
    //echo "</br>";
    arsort($users);

    $count = count($users);
    $text = "<b>👑 Der Nutzer der Woche</b>\n\n";

    if (!empty($users)){

    $username = json_decode(getSite("getChatMember?chat_id=@" . umfragen . "&user_id=" . array_keys($users) [0]) , true) ["result"]["user"]["first_name"];
    $text = $text . '<a href="tg://user?id=' . array_keys($users) [0] . '">' . $username . "</a> mit <b>" . $users[array_keys($users) [0]] . "</b> Umfragen";


    $text = $text . "\n\nWeitere Platzierungen\n";
    for ($i = 1;$i < $count and $i < 3;$i++)
    {
        $text = $text . '<a href="tg://user?id=' . array_keys($users) [$i] . '">' . json_decode(getSite("getChatMember?chat_id=@" . umfragen . "&user_id=" . array_keys($users) [$i]) , true) ["result"]["user"]["first_name"] . "</a> mit " . $users[array_keys($users) [$i]] . " Umfragen";
    }

    $text = str_replace("|", "\\|", $text);

    sendMessage(umfragen, $text);
    }

    //print for Quiz
    $sql = "SELECT * FROM activity WHERE chat = '" . quiz . "' AND time > " . (time() - 604800);
    //echo $sql."</br>";
    $users = array();
    foreach ($db->query($sql) as $row)
    {
        //echo $row["userid"].": ".$row["xp"]."</br>";
        if (!isset($users[$row["userid"]]))
        {
            $users[$row["userid"]] = 1;
        }
        else
        {
            $users[$row["userid"]] = $users[$row["userid"]] + 1;
        }

    }
    //echo "</br>";
    arsort($users);

    $count = count($users);
    $text = "<b>👑 Der Nutzer der Woche</b>\n\n";
 if (!empty($users))
    {

        $username = json_decode(getSite("getChatMember?chat_id=@" . quiz . "&user_id=" . array_keys($users) [0]) , true) ["result"]["user"]["first_name"];
        $text = $text . '<a href="tg://user?id=' . array_keys($users) [0] . '">' . $username . "</a> mit <b>" . $users[array_keys($users) [0]] . "</b> Umfragen";

    $text = $text . "\n\nWeitere Platzierungen\n";
    for ($i = 1;$i < $count and $i < 3;$i++)
    {
        $text = $text . '<a href="tg://user?id=' . array_keys($users) [$i] . '">' . json_decode(getSite("getChatMember?chat_id=@" . quiz . "&user_id=" . array_keys($users) [$i]) , true) ["result"]["user"]["first_name"] . "</a> mit " . $users[array_keys($users) [$i]] . " Umfragen";
    }

    $text = str_replace("|", "\\|", $text);

    sendMessage(quiz, $text);
}

    //echo $text;
    //Print XP
    $sql = "SELECT * FROM activity";
    //echo $sql."</br>";
    $stats = array();
    foreach ($db->query($sql) as $row)
    {
        //echo $row["userid"].": ".$row["xp"]."</br>";
        if (isset($stats[$row["userid"]]))
        {
            $stats[$row["userid"]] = $stats[$row["userid"]] + $row["xp"];
        }
        else
        {
            $stats[$row["userid"]] = $row["xp"];
        }

    }
    //echo "</br>";
    arsort($stats);

    $count = count($stats);
    $text = "<b>📄 Allgemeines Punktebord</b>\n\n";

    for ($i = 0;$i < $count;$i++)
    {
        $text = $text . json_decode(getSite("getChatMember?chat_id=@".umfragen."&user_id=" . array_keys($stats) [$i]) , true) ["result"]["user"]["first_name"] . ": " . $stats[array_keys($stats) [$i]] . "\n";
    }

    $text = str_replace("|", "\\|", $text);
    sendMessage(umfragen, $text);
    sendMessage(quiz, $text);
    sendMessage(diskussionsgruppe, $text);

    //echo $text;
    $db = null;
}

function addUserConsent($userid)
{
    $db = getdb();

    $statement = $db->prepare("INSERT INTO consent (userid) VALUES (?)");
    $statement->execute(array(
        $userid
    ));
    $db = null;

}

function removeUserConsent($userid)
{
    $db = getdb();

    $statement = $db->prepare("DELETE FROM consent WHERE userid = ?");
    $statement->execute(array(
        $userid
    ));
    $statement = $db->prepare("DELETE FROM activity WHERE userid = ?");
    $statement->execute(array(
        $userid
    ));
    $db = null;
}

function checkUserConsent($userid)
{
    $db = getdb();

    $sql = "SELECT * FROM consent WHERE userid = '" . $userid . "'";

    $statement = $db->prepare($sql);
    $statement->execute();
    $row = $statement->fetch();

    $db = null;

     return (!empty($row));

}

function datarequest($m)
{
    $text = "<b>📦 Deine Daten</b>\n\nZustimmung zur Erhebung, Verarbeitung und Speicherung von Aktivitätsdaten:\n";
    if (checkUserConsent($m["from"]["id"]))
    {
        $text = $text . " <b>✔️ Erteilt</b>";
    }
    else
    {
        $text = $text . " <b>❌ Nicht erteilt oder widersprochen</b>";
    }
    $text = $text . "\n\n<i>Hinweis: Nur erteilte Zustimmung ist vermerkt. Der Widerspruch wird aus dem Nicht-Vorhandensein der Zustimmung geschlussfolgert.</i>\n\n<u>Gespeicherte Aktivitätsdatensätze</u>\n\n<pre>Chat | Nachrichtennummer | Zeit | Erfahrungspunkte\n";
    $db = getdb();

    $sql = "SELECT * FROM activity WHERE userid = '" . $m["from"]["id"] . "'";
    foreach ($db->query($sql) as $row)
    {
        $text = $text . $row["chat"] . " | " . $row["messageid"] . " | " . date("d.m.Y H:i:s", $row["time"]) . " | " . $row["xp"] . "\n";
    }
    $text = $text . '</pre> \n\n<i>Hinweis: Die Zeit wird in Form von Unix-Zeitstempeln gespeichert. Um die Lesbarkeit zu verbessern wird dieser formatiert ausgegeben im Format [Tag des Monats].[Monat als Zahl].[Jahr] [Stunden]:[Minuten]:[Sekunden]. Für mehr Informationen verwende /info .</i>';
    //echo
    sendPrivateMessage($m["chat"]["id"], $text);
}

function sendstats($m)
{
    $db = getdb();

    $text = "<b>📈 Deine Statistiken</b>\n\n<u>Erfahrungspunkte</u>\n\n";

    //Umfragen
    $sql = "SELECT * FROM activity WHERE userid = '" . $m["from"]["id"] . "' AND chat = '" . umfragen . "'";
    $singlexp = 0;
    $polls = 0;
    $xp = 0;
    foreach ($db->query($sql) as $row)
    {
        $singlexp = $singlexp + $row["xp"];
        $polls++;
    }
    $xp = $singlexp;
    $text = $text . "Umfragegruppe: " . $singlexp . "\n";

    //Quiu
    $sql = "SELECT * FROM activity WHERE userid = '" . $m["from"]["id"] . "' AND chat = '" . quiz . "'";
    $singlexp = 0;
    $quizes = 0;
    foreach ($db->query($sql) as $row)
    {
        $singlexp = $singlexp + $row["xp"];
        $quizes++;
    }
    $xp = $xp + $singlexp;
    $text = $text . "Quizgruppe: " . $singlexp . "\n";

    //Diskussion
    $sql = "SELECT * FROM activity WHERE userid = '" . $m["from"]["id"] . "' AND chat = '" . diskussionsgruppe . "'";
    $singlexp = 0;
    $mess = 0;
    foreach ($db->query($sql) as $row)
    {
        $singlexp = $singlexp + $row["xp"];
        $mess++;
    }
    $xp = $xp + $singlexp;
    $text = $text . "Diskussionsgruppe - " . $singlexp . "\n";
    $text = $text . "► Gesamt: " . $xp . "\n\n<u>Aktivitäten</u>\n\nUmfragen: " . $polls . "\nQuize: " . $quizes . "\nDisskussionsbeiträge: " . $mess . "\n► Gesamt: " . ($polls + $quizes + $mess);

    //echo
    sendPrivateMessage($m["chat"]["id"], $text);
    $db = null;
}

function addAction($userid, $time, $group, $messageid,$verification)
{

    $db = getdb();

    $statement = $db->prepare("INSERT INTO actions VALUES (?, ?, ? , ?, ?)");
    $statement->execute(array(
        $userid,
        $group,
        $time,
        $messageid,
        $verification
    ));
    $db = null;
}


function getActionUserid($group, $messageid)
{
    $db = getdb();

    $sql = "SELECT * FROM actions WHERE groupname = '" . $group . "' AND message_id = '" . $messageid . "' AND (method = 'consent' OR method = 'manuell')";

    $statement = $db->prepare($sql);
    $statement->execute();
    $row = $statement->fetch();

    $db = null;
    return $row["userid"];
}
function userManuellVerification($userid){
     $db = getdb();

    $sql = "SELECT * FROM actions WHERE userid = '" . $userid . "' AND method = 'manuell'";

    $statement = $db->prepare($sql);
    $statement->execute();
    $row = $statement->fetch();

    $db = null;

     return (!empty($row));
}

function getActionConsentMessage($userid)
{
    $db = getdb();

    $sql = "SELECT * FROM actions WHERE userid = '" . $userid . "' AND (method = 'consent' OR method = 'manuell')";

    $statement = $db->prepare($sql);
    $statement->execute();
    $row = $statement->fetch();

    $db = null;
    return "https://t.me/".$row["groupname"]."/".$row["message_id"];
}
function removeAction($group, $messageid)
{
    $db = getdb();

    $statement = $db->prepare("DELETE FROM actions WHERE groupname = ? AND message_id = ?");
    $statement->execute(array(
        $group,
        $messageid
    ));
    $db = null;
}

?>
