<?php

const diskussionsgruppe = "umfragendiskussion";
const umfragen = "umfragenneu";
const quiz = "quizde";
const testchannel = "-1001418498607";
const userwelcomeumfragen = "Bitte lies dir zunächst die folgende Nachricht durch.\n\n<b>📊 Wozu dient diese Gruppe?</b>\n\nZiel dieser Gruppe ist as Erstellen und Beantworten von Umfragen. Zum Erstellen von Umfragen, beachte bitte diesen kleinen <a href='https://telegra.ph/Richtlinien-und-Zweck-der-Umfragen-Gruppe-05-13'>Leitfaden</a>(<a href='https://t.me/umfragenneu/1163'>in einer Telegram Nachricht anschauen</a>)\n\n<b>💬 Gespräche und Austausch zu den Umfragen</b>\n\nUm die Gruppe übersichtlich zu gestalten finden alle Gespräche in unserer <a href='https://t.me/umfragendiskussion'>Diskussionsgruppe</a> statt.\n\n<b>❔ Quizze</b>\n\nDu möchtest nicht nur Umfragen erstellen oder beantworten, sondern auch Quizze? Dann komm in unsere <a href='https://t.me/quizde'>Quizgruppe</a>!\n\n<b>👑 Belohnung für Aktivität</b>\n\nJede Woche wird der Nutzer mit den meisten Umfragen bzw. Quizzen in der jeweiligen Gruppe gekürt. Hinzu gibt es ein Punktesystem, das für jede Nachricht Punkte verteilt. Die Teilnahme am Punktesystem erfordert eine Aufzeichnung von Daten zu Nachrichten und ist freiwillig.  Die Zustimmung kann jederzeit widerrufen sowie die gesammelten Daten gelöscht werden. Für mehr Informationen schreibe unseren Bot (https://t.me/umfragenetzerktestbot) privat an. Mit dem Verlassen der Gruppen gilt die Zustimmung als entzogen.\n\nDies und weitere Informationen findest Du in der angepinnten Nachricht. Bei Fragen kannst du jederzeit einen der Admins anschreiben.\n\n";
const userwelcomequiz = "Bitte lies dir zunächst die folgende Nachricht durch.\n\n<b>❔ Wozu dient diese Gruppe?</b>\n\nZiel dieser Gruppe ist as Erstellen und Beantworten von Quizzen.\n\n<b>💬 Gespräche und Austausch zu den Quizzen</b>\n\nUm die Gruppe übersichtlich zu gestalten finden alle Gespräche in unserer <a href='https://t.me/umfragendiskussion'>Diskussionsgruppe</a> statt.\n\n<b>📊 Umfragen</b>\n\nDu möchtest nicht nur Quizze erstellen oder beantworten, sondern auch Umfragen? Dann komm in unsere <a href='https://t.me/umfragenneu'>Umfragegruppe</a>!\n\n<b>👑 Belohnung für Aktivität</b>\n\nJede Woche wird der Nutzer mit den meisten Quizzen bzw. Umfragen in der jeweiligen Gruppe gekürt. Hinzu gibt es ein Punktesystem, das für jede Nachricht Punkte verteilt. Die Teilnahme am Punktesystem erfordert eine Aufzeichnung von Daten zu Nachrichten und ist freiwillig.  Die Zustimmung kann jederzeit widerrufen sowie die gesammelten Daten gelöscht werden. Für mehr Informationen schreibe unseren Bot (https://t.me/umfragenetzwerktestbot) privat an. Mit dem Verlassen der Gruppen gilt die Zustimmung als entzogen.\n\nDies und weitere Informationen findest Du in der angepinnten Nachricht. Bei Fragen kannst du jederzeit einen der Admins anschreiben.\n\n";
const userwelcomediskussion = "Bitte lies dir zunächst die folgende Nachricht durch.\n\n<b>💬 Wozu dient diese Gruppe?</b>\n\Diese Gruppe dient zum Austausch über Umfragen und Quizze aus der Umfrage und Quizgruppe\n\n<b>📊 Umfragen</b>\n\n<a href='https://t.me/umfragenneu'>Umfragen</a>\n\n<b>❔ Quizze</b>\n\<a href='https://t.me/quizde'>Quizgruppe</a>!\n\n<b>👑 Belohnung für Aktivität</b>\n\nJede Woche wird der Nutzer mit den meisten Umfragen bzw. Quizzen in der jeweiligen Gruppe gekürt. Hinzu gibt es ein Punktesystem, das für jede Nachricht Punkte verteilt. Die Teilnahme am Punktesystem erfordert eine Aufzeichnung von Daten zu Nachrichten und ist freiwillig.  Die Zustimmung kann jederzeit widerrufen sowie die gesammelten Daten gelöscht werden. Für mehr Informationen schreibe unseren Bot (https://t.me/umfragenetzwerktestbot) privat an. Mit dem Verlassen der Gruppen gilt die Zustimmung als entzogen.\n\nDies und weitere Informationen findest Du in der angepinnten Nachricht. Bei Fragen kannst du jederzeit einen der Admins anschreiben.\n\n";
const verificationconsent = "Um dich freizuschalten, wähle bitte aus, ob Du deine Zustimmung zur Teilnahme am Punktesystem und am Nutzer der Woche erteilst. Die Zustimmung gilt für alle Gruppen. Du hast dazu zwischen 30 und 60 Minuten Zeit, danach wirst du gekickt, kannst aber jederzeit wieder beitreten.";
const verification_consent_asked = "Um dich freizuschalten, folge bitte den Anweisungen in dieser ";
const verification_consent_private = "Da dein Name Symbole oder Worte enthält, die öfters bei automatisch gesteuerten Benutzern(Userbots) auftreten, bitten wir dich den <a href='https://t.me/umfragenetzwerktestbot'>Bot</a> privat anzuschreiben und eine Aufgabe zu lösen, um zu bestätigen, dass du wirklich ein Mensch bist.";
const text_info = "1. Was ist dieser Bot und wozu dient er?\n\nDieser Bot ist der dem „Umfragenetzwerk“ (die Gruppen @umfragenneu , @quizde und @umfragendiskussion  umfassend) zugehörige Bot. Der Bot dient in erster Linie der Messung der Aktivität und davon ausgehend der Erstellung des Punktebords und der Ernennung des Nutzers der Woche. Des Weiteren nimmt der Bot die Aufgabe des Schutzes vor Spam und der Weiterleitung von Nachrichten und Quizzen in die Diskussionsgruppe wahr und bietet die Möglichkeit vorgefertigte Antwortmöglichkeiten zu verwenden.\n\n2. Wie kann ich Umfragen mit vorgefertigten Antwortmöglichkeiten erstellen?\n\nUm Umfragen mit vorgefertigten Antwortmöglichkeiten zu erstellen, sende eine Nachricht mit dem entsprechenden Präfix und der Frage nach einem Leerzeichen. Beispiel:\n\n.jn Gefällt dir dieser Bot?\n\nDie Präfixe sind:\n\n.jn – Antwortmöglichkeiten: „Ja“ und „Nein“\n.z – Antwortmöglichkeiten: „Stimme zu“, „Stimme eher zu“, „Neutral“, „Stimme eher nicht zu“, „Stimme nicht zu“\n.zha – Antwortmöglichkeiten: „Stimme zu“, „Stimme eher zu“, „Stimme eher nicht zu“, „Stimme nicht zu“\n\n\n3. Wie funktioniert das Punktesystem und der Nutzer der Woche?\n\n\nJede Nachricht, die in eine der genannten Gruppen versendet wird, wird an den Bot weitergeleitet und insofern eine Zustimmung vorliegt die Punkte eingetragen. Die Punktwerte werden werden nach Gruppen unterschiedlich errechnet. In der Umfrage- und Quizgruppe wird folgende Formel verwendet:\n\nMinuten seit der letzten Nachricht (max. 180)\n---------------------------------------------------------- x Grundwert\n180\n\nDer Grundwert beträgt in der Umfragegruppe 100 Punkte pro Umfrage, in der Quizgruppe 130 Punkte pro Umfrage.\n\nIn der Diskussionsgruppe erhält der Nutzer pro Wort 2 Punkte, bis maximal 130 Punkten innerhalb eines Zeitrahmens von 60 Minuten.\n\nDas Punktesystem ist für alle Gruppen einheitlich und soll langfristig Aktivitäten auszeichnen.\n\nDen Titel des Nutzers der Woche erhält derjenige, der am meisten Umfragen, Quizze oder Wörter in der jeweiligen Gruppe in der Woche verfasst hat.\n\nHinweis: Punkte für gelöschte oder veränderte Nachrichten werden spätestens am Ende der Woche korrigiert. Davon ausgehend sich verändernde Punktwerte werden nicht berücksichtigt.";
const text_info2 = "4. Welche Daten erhebt, verarbeitet und speichert der Bot und zu welchem Zweck?\n\nWenn ein Nutzer eine Nachricht in eine der genannten Gruppen oder dem Bot in einer privaten Konversation eine Nachricht sendet (dies ist auch z.B. bei Bei- oder Austritten von Gruppen der Fall) oder bearbeitet sendet Telegram ein Update an den den Bot, das Informationen zum Nutzer (Identifikationsschlüssel, bei Telegram eingetragener Vorname, bei Telegram eingetragener Name, Nutzername [insofern dieser vorhanden ist], IETF language tag) und zur Nachricht(Identifikationsschlüssel für Konversation und Nachricht, Inhalt der Nachricht) enthält. Diese Daten sind für die grundlegende Funktion des Bots notwendig.\n\nZum Zwecke des Spamschutzes wird beim Beitreten in die Gruppe der vollständige Name und Nutzername geprüft, sowie der Nutzeridentifikationsschlüssel, der Name der beigetretenen Gruppe, die Zeit, der Identifikationsschlüssel der Willkommensnachricht, sowie die Verifikationsmethode gespeichert, um die Verifizierung der Nutzer durchzuführen. Die Daten werden nach einer erfolgreichen Verifizierung oder dem automatischen Entfernen des Nutzers gelöscht.\n\nFür die Messung der Aktivitäten werden, insofern der Absender der Nachricht seine Zustimmung erteilt hat, Metadaten zu in den Gruppen gesendeten Nachrichten gespeichert. Diese Metadaten umfassen den Nutzeridentifikationsschlüssel, den Identifikationsschlüssel für Konversation und Nachricht, die Zeit und den erteilten Punktewert. Der erteilte Punktewert errechnet sich aus diesen und den Metadaten der vorangegangenen Nachricht. In der Umfrage- und Quizgruppe werden nur Daten zu Nachrichten, die eine Umfrage bzw. ein Quiz enthalten, gespeichert. Diese Daten werden ausschließlich für den Nutzer der Woche, das Punktebord oder Anfragen über den privaten Chat verwendet.\n\nUm zu gewährleisten, dass ausschließlich Daten von Nutzern, die ihre Zustimmung erteilt haben, gespeichert werden, wird eine Liste mit den Identifikationsschlüsseln dieser geführt.\n\nFerner werden Nachrichten und Befehle, welche in der privaten Konversation mit dem Bot, verarbeitet.\n\n5. Kann ich meine Daten löschen oder einsehen?\n\nJa. Um eine Kopie deiner Daten zu erhalten verwende den Befehl /datenauszug . Um deine Daten zu löschen widerspreche der Sammlung dieser mit /widersprechen . Danach kann diese erneut mit /erteilen aktiviert werden. Beim Austreten aus allen Gruppen werden die Daten automatisch gelöscht.\n\n6. Welche Vorkehrungen sind zum Schutz meiner Daten getroffen?\n\nDer Bot kommuniziert zu den Servern von Telegram über eine verschlüsselte Verbindung und führt keine Aufzeichnungen zu den eingehenden Daten. Gespeicherte Daten werden passwortgeschützt in einer Datenbank bewahrt. Ein umfassenderer Schutz ist aufgrund des Ursprungs aus einer öffentlich einsehbaren Gruppe und der Tatsache, dass ausschließlich Metainformationen gespeichert werden, fraglich\n\n5. Ich habe Interesse an technischen Details. Wo kann ich Informationen finden?\n\nDer Bot verwendet die Telegram Bot API und empfängt Updates zu den Typen „message“, „edited_message“ und „callback_query“ über eine Webhook. Der Quellcode ist unter https://bitbucket.org/umfragenetzwerk/tgbot/src offen einsehbar.";
const tooken = "YOURTOOKEN";

//for long polling
function setOffsetposition($pos){
     $myfile = fopen(".offsetpos", "w") or die("Unable to open file!");
    fwrite($myfile, $pos);
    fclose($myfile);
}
function getOffsetposition(){
    return intval(file_get_contents('.offsetpos'))+1;
}

function getSite($url){
    $s = curl_init("https://api.telegram.org/bot".tooken."/".$url);
        curl_setopt($s, CURLOPT_USERAGENT, 'TgUmfrageBot');
        curl_setopt($s, CURLOPT_TIMEOUT, 30);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($s, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($s);

        $content = curl_multi_getcontent($s);
        curl_close($s);
        return $content;
}
function pushSite($url){
     $s = curl_init("https://api.telegram.org/bot".tooken."/".$url);
        curl_setopt($s, CURLOPT_USERAGENT, 'TgUmfrageBot');
        curl_setopt($s, CURLOPT_TIMEOUT, 30);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($s, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($s);
        curl_close($s);
}



function sendPoll($m){
    if($m["chat"]["username"] == umfragen){
    $c = $m["text"];
    //echo $c."</br></br>";
    if(strlen($c) > 5){
    if(substr($c,0,4) == ".jn "){

        $content = getSite('sendPoll?chat_id=@'.$m["chat"]["username"].'&question='.substr($c,4).'&options=["Ja","Nein"]');
        //echo $content."</br></br>";
        sendMessage(umfragen,"Umfrage von ".$m["from"]["first_name"]);
//echo
       getSite("deleteMessage?chat_id=@".$m["chat"]["username"]."&message_id=".$m["message_id"]);
        $newm = json_decode($content,true)["result"];
        $newm["from"]["id"] = $m["from"]["id"];
        addActivity($newm);
        sendMessage(diskussionsgruppe,"Umfrage von ".$m["from"]["first_name"]);


    }else if(substr($c,0,3) == ".z "){

        $content = getSite('sendPoll?chat_id=@'.$m["chat"]["username"].'&question='.substr($c,3).'&options=["Stimme zu","Stimme eher zu","Neutral","Stimme eher nicht zu","Stimme nicht zu"]');
        //echo $content."</br></br>";
        sendMessage(umfragen,"Umfrage von ".$m["from"]["first_name"]);
        pushSite("deleteMessage?chat_id=@".$m["chat"]["username"]."&message_id=".$m["message_id"]);
        $newm = json_decode($content,true)["result"];
        $newm["from"]["id"] = $m["from"]["id"];
        addActivity($newm);
        sendMessage(diskussionsgruppe,"Umfrage von ".$m["from"]["first_name"]);



    }else if(substr($c,0,5) == ".zha "){
     $content = getSite('sendPoll?chat_id=@'.$m["chat"]["username"].'&question='.substr($c,5).'&options=["Stimme zu","Stimme eher zu","Stimme eher nicht zu","Stimme nicht zu"]');
        //echo $content."</br></br>";
        sendMessage(umfragen,"Umfrage von ".$m["from"]["first_name"]);

//echo
        getSite("deleteMessage?chat_id=@".$m["chat"]["username"]."&message_id=".$m["message_id"]);
        $newm = json_decode($content,true)["result"];
        $newm["from"]["id"] = $m["from"]["id"];
        addActivity($newm);
        sendMessage(diskussionsgruppe,"Umfrage von ".$m["from"]["first_name"]);

    }
    }
    }

}

function handleusercallback($callback){
    //echo "<p>".$callback["from"]["id"]."</p>";
   if($callback["from"]["id"] == getActionUserid($callback["message"]["chat"]["username"],$callback["message"]["message_id"])){

        removeRestrictions($callback["from"]["id"]);
        if($callback["data"] == "plus"){
            addUserConsent($callback["from"]["id"]);
            answerCallback($callback["id"],"Herzlichen Glückwunsch! Die Zustimmung gespeichert wurde gespeichert. Du bist nun freigeschaltet.");
        }else{
            answerCallback($callback["id"],"Herzlichen Glückwunsch! Du bist nun freigeschaltet.");
        }
   }else{
        answerCallback($callback["id"],"Du hast dazu keine Berechtigung.");
   }
}

function removeRestrictions($userid){
    $db = getdb();
    $sql = "SELECT * FROM actions WHERE userid = '" . $userid . "'";
    foreach ($db->query($sql) as $row)
    {
        deleteMessage($row["groupname"],$row["message_id"]);
        unrestrictUser($row["groupname"],$userid);
        removeAction($row["groupname"],$row["message_id"]);

    }
    $db = null;
}

const state_user_restricted = 0;
const state_user_new = 1;
const state_user_member = 2;
function handleuserjoin($m){
    if(!checkUserConsent($m["new_chat_participant"]["id"])){

        $state = state_user_new;
        if(umfragen != $m["chat"]["username"]){
            $status = (json_decode(getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["new_chat_participant"]["id"]),true)["result"]["status"]);

            if($status == "member"){
                $state = state_user_member;
            }else if($status == "restricted"){
                $state = state_user_restricted;
            }
        }
        if(diskussionsgruppe != $m["chat"]["username"] AND $state == state_user_new){
            $status = json_decode(getSite("getChatMember?chat_id=@".diskussionsgruppe."&user_id=".$m["new_chat_participant"]["id"]),true)["result"]["status"];

            if($status == "member"){
                $state = state_user_member;
            }else if($status == "restricted"){
                $state = state_user_restricted;
            }
        }
        if(quiz != $m["chat"]["username"] AND $state == state_user_new){
            $status = (json_decode(getSite("getChatMember?chat_id=@".quiz."&user_id=".$m["new_chat_participant"]["id"]),true)["result"]["status"]);

            if($status == "member"){
                $state = state_user_member;
            }else if($status == "restricted"){
                $state = state_user_restricted;
            }
        }


        if($state != state_user_member){
            //echo
             restrictUser($m["chat"]["username"],$m["new_chat_participant"]["id"]);

             $message = "";

             $result = "";
             if($m["chat"]["username"] == umfragen){
                    $message = "Herzlich Willkommen ".$m["new_chat_participant"]["first_name"]." in unserer Umfragegruppe!\n\n".userwelcomeumfragen;
            }else if($m["chat"]["username"] == quiz){
                    $message = "Herzlich Willkommen ".$m["new_chat_participant"]["first_name"]." in unserer Quizgruppe!\n\n".userwelcomequiz;
            }else if($m["chat"]["username"] == diskussionsgruppe){
                    $message = "Herzlich Willkommen ".$m["new_chat_participant"]["first_name"]." in unserer Diskussionsgruppe!\n\n".userwelcomequiz;
            }



            if($state == state_user_restricted){

            $message = $message.verification_consent_asked.'<a href="'.getActionConsentMessage($m["new_chat_participant"]["id"]).'">Nachricht</a>';
            $result = sendMessage($m["chat"]["username"],$message);
            addAction($m["new_chat_participant"]["id"],time()+1800,$m["chat"]["username"],json_decode($result,true)["result"]["message_id"],"awaiting");
            }else if(isSpamAccount($m["new_chat_participant"])){
                $message = $message.verification_consent_private;
                $result = sendMessage($m["chat"]["username"],$message);
                deleteMessage($m["chat"]["username"],["message_id"]);
                addAction($m["new_chat_participant"]["id"],time()+1800,$m["chat"]["username"],json_decode($result,true)["result"]["message_id"],"manuell");
            }else{
                $message = $message.verificationconsent;
                $result = sendMessagewithInline($m["chat"]["username"],$message,'{"inline_keyboard": [[{"text":"Zustimmen und freischalten","callback_data":"plus"}],[{"text":"Ohne Zustimmung freischalten","callback_data":"minus"}]]}');
                addAction($m["new_chat_participant"]["id"],time()+1800,$m["chat"]["username"],json_decode($result,true)["result"]["message_id"],"consent");
            }
        }
    }
}

function handleuserleft($m){
    if(checkUserConsent($m["left_chat_participant"]["id"])){
        $still = false;
        if(umfragen != $m["chat"]["username"]){
            //echo "<p>.".$m["left_chat_participant"]["first_name"]." in group: umfragen</br>".getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["left_chat_participant"]["id"])."</p>";
        $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["left_chat_participant"]["id"]),true)["result"]["status"],array("member","creator","administrator","restricted")));
        }
        if(diskussionsgruppe != $m["chat"]["username"] AND !$still){
                        //echo "<p>.".$m["left_chat_participant"]["first_name"]." in group: diskuss</br>".getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["left_chat_participant"]["id"])."</p>";

        $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".diskussionsgruppe."&user_id=".$m["left_chat_participant"]["id"]),true)["result"]["status"],array("member","creator","administrator","restricted")));
        }
        if(quiz != $m["chat"]["username"] AND !$still){
                        //echo "<p>.".$m["left_chat_participant"]["first_name"]." in group: quiz</br>".getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["left_chat_participant"]["id"])."</p>";

        $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".quiz."&user_id=".$m["left_chat_participant"]["id"]),true)["result"]["status"],array("member","creator","administrator","restricted")));

        }

        if(!$still){
            removeUserConsent($m["left_chat_participant"]["id"]);
            //echo "</br>User consent removed";
        }else{
            //echo "</br>User consent not removed";
        }
    }else{
        $still = false;
        if(umfragen != $m["chat"]["username"]){
            //echo "<p>.".$m["left_chat_participant"]["first_name"]." in group: umfragen</br>".getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["left_chat_participant"]["id"])."</p>";
        $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["left_chat_participant"]["id"]),true)["result"]["status"],array("member","creator","administrator","restricted")));
        }
        if(diskussionsgruppe != $m["chat"]["username"] AND !$still){
                        //echo "<p>.".$m["left_chat_participant"]["first_name"]." in group: diskuss</br>".getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["left_chat_participant"]["id"])."</p>";

        $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".diskussionsgruppe."&user_id=".$m["left_chat_participant"]["id"]),true)["result"]["status"],array("member","creator","administrator","restricted")));
        }
        if(quiz != $m["chat"]["username"] AND !$still){
                        //echo "<p>.".$m["left_chat_participant"]["first_name"]." in group: quiz</br>".getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["left_chat_participant"]["id"])."</p>";

        $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".quiz."&user_id=".$m["left_chat_participant"]["id"]),true)["result"]["status"],array("member","creator","administrator","restricted")));

        }

        if(!$still){
            removeRestrictions($m["left_chat_participant"]["id"]);
        }
            //echo "</br>No consent was given. Id: ".$m["left_chat_participant"]["id"];

    }
}

function handleprivate($m){
    //echo "<p>Private: ".json_encode($m)."</p>";
    if(isset($m["text"])){
        $t = $m["text"];
        //echo "<p>".$t."</p>";
        if($t == "/erteilen"){
            consentRequested($m);
        }else if($t == "/widersprechen"){
            consentRemoveRequested($m);
        }else if($t == "/info"){
            info($m);
        }else if($t == "/datenauszug"){
            datarequest($m);
        }else if($t == "/statistiken"){
            sendstats($m);
        }else if($t == "/berichtigung"){
            correctsingleUserdata($m);
        }
        else if($t == "/help"){
            help($m);
        }
        else if(userManuellVerification($m["from"]["id"])){
            doPrivateChapta($m);
        }
        else{
            help($m);
        }
    }else{
    help($m);
    }
}

function doPrivateChapta($m){
    if($m["text"] == "5"){
        removeRestrictions($m["from"]["id"]);
        sendPrivateMessage($m["chat"]["id"],"✔️ Herzlich Glückwunsch. Du bist nun in freigeschaltet. Um am Erfahrungsbord und am Nutzer der Woche teilzunehmen verwende bitte den Befehl /erteilen.");

    }else if($m["text"] == "/start"){
        sendPrivateMessage($m["chat"]["id"],"<b>🔢 Verifizierung</b>\n\nUm dich freizuschalten, löse bitte die folgende Aufgabe und schicke die Antwort als Ziffer.\n\n<b>Beispiel:</b>e87in s  p lu73s  zw?e =i\n<b>Antwort:</b> 3\n\n<b>Aufgabe:</b> z4w?e i  p lu0s  d r5e=i");
    }else{
        sendPrivateMessage($m["chat"]["id"],"❌ Falsche Antwort.  Bitte versuche es erneut.\n\nFalls du Probleme hast die Aufgabe zu lösen, kontaktiere einen der Admins um dich manuell freizuschalten.\n\n<b>Beispiel:</b>e87in s  p lu73s  zw?e =i\n<b>Antwort:</b> 3\n\n<b>Aufgabe:</b> z4w?e i  p lu0s  d r5e=i");
    }
}

function correctsingleUserdata($m){

sendPrivateMessage($m["chat"]["id"],"⏳ Überprüfe deine gesendeten Nachrichten. Dies kann unter Umständen einen kleinen Moment dauern.");

    $db = getdb();


$sql = "SELECT * FROM activity WHERE userid = '".$m["from"]["id"]."'";
    //echo $sql."</br>";
$count = 0;
foreach ($db->query($sql) as $row) {
    if(!json_decode(forwardMessagetoChannel($row["chat"],$row["messageid"]),true)["ok"]){
        delAct($row["messageid"],$row["chat"]);
        $count++;
    }
}

sendPrivateMessage($m["chat"]["id"],"☑️ Deine Daten wuden berichtigt. Gelöschte Datensätze: ".$count);

$db = null;
}
function consentRequested($m){
    if(!checkUserConsent($m["from"]["id"])){
        $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".umfragen."&user_id=".$m["from"]["id"]),true)["result"]["status"],array("member","creator","administrator")));
        if(!$still){
            $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".diskussionsgruppe."&user_id=".$m["from"]["id"]),true)["result"]["status"],array("member","creator","administrator")));
        }
        if(!$still){
            $still = (in_array(json_decode(getSite("getChatMember?chat_id=@".quiz."&user_id=".$m["from"]["id"]),true)["result"]["status"],array("member","creator","administrator")));
        }

        if($still){
            addUserConsent($m["from"]["id"]);
            sendPrivateMessage($m["chat"]["id"],"☑️ Die Zustimmung wurde gespeichert. Du nimmst ab sofort am Erfahrungsbord und am Nutzer der Woche teil. Falls Du Dich umentschieden hast, kannst du mit /widersprechen der weiteren Sammlung von Aktivitätsdaten widersprechen und die bisher erhobenen Daten löschen lassen.");
        }else{
        sendPrivateMessage($m["chat"]["id"],"ℹ️ Um zustimmen zu können musst du in einer der folgenden Gruppen sein: @".umfragen." , @".quiz." @".diskussionsgruppe." .");
        }
    }else{
        sendPrivateMessage($m["chat"]["id"],"ℹ️ Du hast bereits der der Messung der Aktivitäten zugestimmt. Um die Zustimmung aufzuheben verwende bitte /widersprechen .");
    }
}

function consentRemoveRequested($m){
    if(checkUserConsent($m["from"]["id"])){
            removeUserConsent($m["from"]["id"]);
            sendPrivateMessage($m["chat"]["id"],"☑️ Die Zustimmung wurde widerrufen. Deine Nutzeridentifikationsnummer wurde aus unserer Datenbank gelöscht. Mit dieser Nummer verknüpfte Aktivitätsdaten wurden gelöscht. Du nimmst ab sofort nicht am Erfahrungsbord und am Nutzer der Woche teil. Falls Du Dich umentschieden hast, kannst du mit /erteilen der weiteren Sammlung von Aktivitätsdaten zustimmen.");

    }else{
        sendPrivateMessage($m["chat"]["id"],"ℹ️ Du hast bereits der der Messung der Aktivitäten widersprochen oder befindest dich in keiner der verknüpften Gruppen. Um die Zustimmung zu erteilen verwende bitte /erteilen . Du kannst das jederzeit rückgängig machen und die Daten löschen lassen.");
    }
}

function info($m){
        sendPrivateMessage($m["chat"]["id"],text_info);
        sendPrivateMessage($m["chat"]["id"],text_info2);
}
function help($m){
        //echo
         sendPrivateMessage($m["chat"]["id"],"ℹ️ Verwende bitte einen der folgenden Befehle:\n/erteilen - Erteilt Zustimmung zur Aktivitätenmessung\n/widersprechen - Widerpsricht einer Aktivitätenmessung\n/datenauszug - Sendet alle mit deinem Nutzer verknüpften Daten\n/statistiken - Zeigt interessante Statistiken an\n/berichtigung - Löscht die Information über bereits in Telegram gelöschte Nachrichten\n/info - Zeigt Informationen an.");

}
function isSpamAccount($acc){
    $keywords = array("🔞","😍","💲","😘","💋","sweet","sex","cute","girl","xxx","naughty","fuck","teen","bitcoin","crypto","trade","trading","stock","market","buy","sale","money","will","schwanz","geil","ficken","❤","😘");
    $idstring = $acc["first_name"];
    if(isset($acc["last_name"])){
        $idstring = $idstring." ".$acc["last_name"];
    }
    if(isset($acc["username"])){
        $idstring = $idstring." ".$acc["username"];
    }
    $idstring = strtolower($idstring);

    foreach ($keywords as $key) {
    if (strpos($idstring, $key) !== FALSE) {
        return true;
    }
}
return false;
}

?>