<?php

function forewardMessage($message){
    echo "</br></br>".getSite("forwardMessage?chat_id=@umfragendiskussion&from_chat_id=@".$message["chat"]["username"]."&message_id=".$message["message_id"]);
}
function forwardMessagetoChannel($chat,$message_id){
return getSite("forwardMessage?chat_id=".testchannel."&from_chat_id=@".$chat."&message_id=".$message_id);
}
function sendMessage($chat,$text){
    return getSite("sendMessage?chat_id=@".$chat."&parse_mode=html&text=".urlencode($text));
}
function sendMessagewithInline($chat,$text,$inline){
    return getSite("sendMessage?chat_id=@".$chat."&parse_mode=html&text=".urlencode($text)."&reply_markup=".$inline);
}
function sendPrivateMessage($chat,$text){
    return getSite("sendMessage?chat_id=".$chat."&parse_mode=html&text=".urlencode($text));
}
function restrictUser($chat,$userid){
    return getSite('restrictChatMember?chat_id=@'.$chat.'&user_id='.$userid.'&permissions={"can_send_messages" : false}');
}
function unrestrictUser($chat,$userid){
    return getSite('restrictChatMember?chat_id=@'.$chat.'&user_id='.$userid.'&permissions={"can_send_messages" : true, "can_send_media_messages": true, "can_send_polls": true, "can_send_other_messages": true, "can_add_web_page_previews" : true, "can_change_info": true, "can_invite_users" : true, "can_pin_messages": true}');
}
function deleteMessage($chat,$id){
    return getSite("deleteMessage?chat_id=@".$chat."&message_id=".$id);
}
function answerCallback($callback_id,$text){
    return getSite("answerCallbackQuery?show_alert=true&callback_query_id=".$callback_id."&text=".$text);
}
function banChatMember($userid,$chat_id){
    return getSite("kickChatMember?until_date=0&chat_id=@".$chat_id."&user_id=".$userid);
}
function unbanChatMember($userid,$chat_id){
    return getSite("unbanChatMember?chat_id=@".$chat_id."&user_id=".$userid);
}

?>